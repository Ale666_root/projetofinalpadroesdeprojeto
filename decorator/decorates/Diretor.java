package decorator.decorates;


import decorator.Professores;
import decorator.ProfessoresDecorate;

public class Diretor extends ProfessoresDecorate {
    public Diretor(Professores umProfessor) {
        super(umProfessor);
        nome = "Diretor";
        salario = 10000;
    }
}
