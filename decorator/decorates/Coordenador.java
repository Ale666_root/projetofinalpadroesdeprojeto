package decorator.decorates;


import decorator.Professores;
import decorator.ProfessoresDecorate;

public class Coordenador extends ProfessoresDecorate {
    public Coordenador(Professores umProfessor) {
        super(umProfessor);
        nome = "Coordenador";
        salario = 6399;
    }
}
