package decorator;


import decorator.decorates.Coordenador;
import decorator.decorates.Diretor;
import decorator.decorates.gerenteDeEnsino;

public class Main {
    public static void main(String[] args){
        Diretor diretor = new Diretor(new Professor());
        Coordenador coordenador = new Coordenador(new Professor());
        gerenteDeEnsino gerentedeensino = new gerenteDeEnsino(new Professor());

        System.out.println();

        System.out.println(diretor.getNome());

        System.out.println(diretor.getSalario());

        System.out.println();

        System.out.println(coordenador.getNome());

        System.out.println(coordenador.getSalario());

        System.out.println();

        System.out.println(gerentedeensino.getNome());

        System.out.println(gerentedeensino.getSalario());


    }

}

