package decorator;



public abstract class Professores {
    protected String nome;
    protected double salario;

    public String getNome() {
        return nome;
    }
    public double getSalario() {
        return salario;
    }

}
