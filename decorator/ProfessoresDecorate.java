package decorator;


public abstract class ProfessoresDecorate extends Professores {
    Professores professores;

    public ProfessoresDecorate(Professores umProfessor) {
        professores = umProfessor;
    }

    @Override
    public String getNome() {
        return professores.getNome() + " " + professores.getSalario() + " + " + nome + " " + salario;
    }

    public double getSalario() {
        return professores.getSalario() + salario;
    }

}