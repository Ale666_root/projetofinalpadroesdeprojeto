package Command;

import java.util.ArrayList;
import java.util.List;

/** Classe invocadora */
public class Experimentos implements Cloneable {

    private List<RoboAction> comandos = new ArrayList<>();

    public void Execute(RoboAction roboAction) {
        this.comandos.add(roboAction); // optional
        roboAction.execute();
    }


    // This method calls Object's clone().
    public Experimentos getClone() {
        try {
            // chamada para clonar objeto
            return (Experimentos) super.clone();
        } catch (CloneNotSupportedException e) {
           // System.out.println(" Clonagem não permeitida ");
            return this;
        }
    }

    @Override
    public String toString() {
        return "Experimentos{" +
                "comandos=" + comandos +
                '}';
    }
}
