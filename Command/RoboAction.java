package Command;

public interface RoboAction {
    void execute();
}
