package Command;

/** O Command para desligar a luz - ConcreteCommand #2 */
public class AdicionarProdutos implements RoboAction {
    private Robo  robo;

    public AdicionarProdutos(Robo robo) {
        this.robo =robo ;
    }

    @Override    // Command
    public void execute() {
        robo.AdicionarProdutos();
    }
}
