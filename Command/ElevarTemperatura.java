package Command;

//** O Command para ligar a luz - ConcreteCommand #1 */
public class ElevarTemperatura implements RoboAction {
    private Robo theLight;

    public ElevarTemperatura(Robo robo) {
        this.theLight = robo;
    }

    @Override    // Command
    public void execute() {
        theLight.AumentarTemperatura();
    }

}
