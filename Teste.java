import Command.*;
import Observer.ConsomeNoticia;
import Observer.NoticiarioAssina;
import Observer.User;
import decorator.Professor;
import decorator.decorates.Coordenador;
import decorator.decorates.Diretor;
import decorator.decorates.gerenteDeEnsino;

import java.util.Scanner;

public class Teste {
    public static void main(String[] args) {
        Scanner ler = new Scanner(System.in);
        int opcao = 1;
        // Experimentos experimentosClone = new Experimentos[1];
        while (opcao != 0) {
            System.out.println("digite uma opção [0] sairc[2] padrão command [3] decorator  [4]   observer");
            opcao = ler.nextInt();

            switch (opcao) {
                case 2:
                    TestarCommand();
                    break;
                case 3:
                    TestarDecorator();
                    break;
                case 4:
                    TestarObserver();
                    break;
            }
            System.out.println("finalizado");
        }
    }

    public static void TestarCommand() {
        Robo robo = new Robo();
        RoboAction command = new ElevarTemperatura(robo);
        RoboAction command11 = new AdicionarProdutos(robo);
        Experimentos experimentos = new Experimentos();

        Experimentos experimentos2;

        experimentos2 = experimentos.getClone();
        experimentos.Execute(command);
        experimentos.Execute(command11);
        System.out.println(experimentos2.toString());
    }

    public static void TestarDecorator() {
        Diretor diretor = new Diretor(new Professor());
        Coordenador coordenador = new Coordenador(new Professor());
        gerenteDeEnsino gerentedeensino = new gerenteDeEnsino(new Professor());
        System.out.println();
        System.out.println(diretor.getNome());
        System.out.println(diretor.getSalario());
        System.out.println();
        System.out.println(coordenador.getNome());
        System.out.println(coordenador.getSalario());
        System.out.println();
        System.out.println(gerentedeensino.getNome());
        System.out.println(gerentedeensino.getSalario());
    }

    public static void TestarObserver() {
        NoticiarioAssina noticiarioAssina = new NoticiarioAssina();
        ConsomeNoticia user = new User("julioa", noticiarioAssina);
        ConsomeNoticia user2 = new User("fabio", noticiarioAssina);
        noticiarioAssina.novaNoticia("Dolar em alta", 2, 11, "economia");
        ConsomeNoticia user3 = new User("tadeu", noticiarioAssina);
        noticiarioAssina.novaNoticia("brexit adiado uniao europeia", 12, 10, "politica");
    }
}