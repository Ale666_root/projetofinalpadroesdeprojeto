package Observer;


public class User implements ConsomeNoticia {
    private String nome;

    public User(String nome,Subject subject){
        this.nome=nome;
        subject.cadastrarUser(this);
    }

    //Método que recebe as novas notificações do usuario
    @Override
    public void notificaNoticia(String TextoNoticia, int dia, int mes, String topico) {
        System.out.println(nome + TextoNoticia + dia + "/" + mes + topico);
    }

}
