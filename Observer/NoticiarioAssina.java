package Observer;


import java.util.ArrayList;
import java.util.List;

public class NoticiarioAssina implements Subject {
    private String TextoNoticia, topico;
    private int dia, mes;
    private List<ConsomeNoticia> consomeNoticias;

    //Método que cadastra uma nova tocia e chama o notificar para notificar os usuarios cadastrados
    public void novaNoticia(String TextoNoticia, int dia, int mes, String topico) {
        this.TextoNoticia = TextoNoticia;
        this.dia = dia;
        this.mes = mes;
        this.topico = topico;
        //Chamando método notificar
        this.notificar();
    }


    public NoticiarioAssina(){
        this.consomeNoticias =new ArrayList<>();
    }

    public List<ConsomeNoticia> getConsomeNoticias() {
        return consomeNoticias;
    }

    public void setConsomeNoticias(List<ConsomeNoticia> consomeNoticias) {
        this.consomeNoticias = consomeNoticias;
    }
    @Override
    public void cadastrarUser(ConsomeNoticia o) {
        this.consomeNoticias.add(o);

    }

    @Override
    public void removerrUser(ConsomeNoticia o) {
        int id = this.consomeNoticias.indexOf(o);
        if(id > 0){
            this.consomeNoticias.remove(id);
        }
    }



    //Método para notificar os usuarios cadastrados
    @Override
    public void notificar() {
        for (ConsomeNoticia user: consomeNoticias) {
            user.notificaNoticia(" Nova noticia cadastrada: "+ this.TextoNoticia + ". Dia e Mês: ", this.dia, this.mes, ". Tópico: " + this.topico);

        }
    }
}