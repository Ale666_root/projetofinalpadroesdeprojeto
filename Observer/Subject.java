package Observer;


public interface Subject {
    void cadastrarUser(ConsomeNoticia o);
    void removerrUser(ConsomeNoticia o);
    void notificar();
}
